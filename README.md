# InceptionV3_iNat2017_WaterStress2018
iNaturalist (iNat) 2017 Inception V3: feature_vector.

使用モジュールURL https://www.tensorflow.org/hub/modules/google/inaturalist/inception_v3/feature_vector/1

モジュール説明

InceptionV3_iNaturalist (iNat) 2017

ILSVRC-2012-CLS（「Imagenet」）の事前トレーニング後に、

iNaturalist（iNat）2017データセットのトレーニングによって得られました。

※iNaturalist2017：http://createwith.ai/dataset/20170723/883

自分のホーム上にtmpというデュレクトリを作成する cd /home/simizu mkdir tmp

学習実行例

python3 retrain.py

--how_many_training_steps=4000

--summaries_dir=training_summaries/basic

--bottleneck_dir=bottlenecks

--saved_model_dir=inception

--output_graph=retrained_graph.pb

--output_labels=retrained_labels.txt

--TFHUB_CACHE_DIR=~/tmp

--tfhub_module=https://tfhub.dev/google/inaturalist/inception_v3/feature_vector/1

--image_dir=train1

学習器利用方法

python3 image_test.py \

--image=判別したい画像のデュレクトリ \

--graph=/home/simizu/anaconda3/Water_Stress/inceptionv3_iNat2017_WaterStress/retrained_graph.pb \

--labels=/home/simizu/anaconda3/Water_Stress/inceptionv3_iNat2017_WaterStress/retrained_labels.txt \

Tensorboardの利用方法

--summaries_dir=training_summaries/basicとした場合

training_summariesが保存されているデュレクトリに行き

tensorboard --logdir=training_summaries　

※デュレクトリは環境によって変えてください.


